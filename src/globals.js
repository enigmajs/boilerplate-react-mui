import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {css} from '@emotion/react'

global.React = React;
global.ReactDOM = ReactDOM;
global.PropTypes = PropTypes;
global.css = css;